# Ejercicio Validate Expression Choice

Refer to the exhibits. What payload is logged at the end of the main flow?

![alt text](image.png)

- [x] [payload == 'US']
- [ ] #[payload = 'US']
- [ ] #[ if(payload == 'US')]
- [ ] #[ if(payload = 'US')]

**Respuesta correcta:** [payload == 'US']